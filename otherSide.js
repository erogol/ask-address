var map1Lat = 0;
var map1Lon = 0;
var map2Lat = 0;
var map2Lon = 0;

function initOtherSide() {
	
	$("#map").css("height", "250px");
	$("#map").css("width", "700px");
	$("#map2").css("height", "250px");
	$("#map2").css("width", "700px");

	
	//map.setCenter(new OpenLayers.LonLat(longitude,initLat).transform(new OpenLayers.Projection("EPSG:4326"),map.getProjectionObject()),5);
	setLonLatMap2();
	
	map2 = new OpenLayers.Map('map2');
	map2.addControl(new OpenLayers.Control.LayerSwitcher());
	map2.addControl(new OpenLayers.Control.MousePosition());
	map2.removeControl(map.getControl(0));
	var clicked2 = new OpenLayers.Control.Click();
	var drag2 = new OpenLayers.Control.DragFeature();
	var dragcontrol = new OpenLayers.Control.DragPan({
		'map' : this.mymap,
		'panMapDone' : function(xy) {
			if(this.panned) {
				var res = null;
				if(this.kinetic) {
					res = this.kinetic.end(xy);
				}
				this.map.pan(this.handler.last.x - xy.x, this.handler.last.y - xy.y, {
					dragging : !!res,
					animate : false
				});
				if(res) {
					var self = this;
					this.kinetic.move(res, function(x, y, end) {
						self.map.pan(x, y, {
							dragging : !end,
							animate : false
						});
					});
				}
				this.panned = false;
			}
			this.userdragged = true;
			// do whatever you want here
			setMap2Location();
		}
	});
	dragcontrol.draw();
	map.addControl(dragcontrol);
	dragcontrol.activate();

	map2.addControl(clicked2);
	//map.addControl(drag2);
	// API key for http://openlayers.org. Please get your own at
	// http://bingmapsportal.com/ and use that instead.
	var apiKey = "AtM2t-8VJV5y47RACXJeLB1JSqDKTy36F2kCYqjrcEG7RmP44yJyJ9hA2y5AGjFr";

	var road = new OpenLayers.Layer.Bing({
		name : "Bing Road",
		key : apiKey,
		type : "Road"
	});
	var hybrid = new OpenLayers.Layer.Bing({
		name : "Bing Hybrid",
		key : apiKey,
		type : "AerialWithLabels"
	});
	var aerial = new OpenLayers.Layer.Bing({
		name : "Bing Aerial",
		key : apiKey,
		type : "Aerial"
	});

	var gphy = new OpenLayers.Layer.Google("Google Physical", {
		type : google.maps.MapTypeId.TERRAIN
	});
	var gmap = new OpenLayers.Layer.Google("Google Streets", // the default
	{
		numZoomLevels : 20
	});
	var ghyb = new OpenLayers.Layer.Google("Google Hybrid", {
		type : google.maps.MapTypeId.HYBRID,
		numZoomLevels : 20
	});
	var gsat = new OpenLayers.Layer.Google("Google Satellite", {
		type : google.maps.MapTypeId.SATELLITE,
		numZoomLevels : 22
	});

	map2.addLayers([gmap, gphy, ghyb, gsat,road, hybrid, aerial ]);

	/*if(navigator.geolocation){
	 setTimeout(getCurrentLoc,2500);//to get correct coordinates
	 }else{
	 alert("GeoLocation is not supported");
	 }*/
	map2.setCenter(new OpenLayers.LonLat(map2Lon, map2Lat).transform(new OpenLayers.Projection("EPSG:4326"),map.getProjectionObject()), map.getZoom());

	//use geolocation option of browser and find the current address of user
	
	 //$("overlay").setStyle({'pointer-events': 'none'});
	 
	 changeButtonOtherSide();

}

function setLonLatMap2(){
	var lonlat = map.getCenter().transform(map.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));
	longitude = lonlat.lon;
	latitude = lonlat.lat;
	//console.log(longitude+" "+latitude);
	if(longitude < 0){
		longitude = longitude*-1;
		map2Lon = -1 *(180-longitude);
		map2Lon *= -1;
	}else{
		map2Lon = -1 *(180-longitude);
	}	
	map2Lat = -1* latitude;
}
function setMap2Location() {
	setLonLatMap2();
	//console.log(map2Lon+" "+map2Lat);
	map2.setCenter(new OpenLayers.LonLat(map2Lon,map2Lat).transform(new OpenLayers.Projection("EPSG:4326"),map.getProjectionObject()), map.getZoom());
}

function changeButtonOtherSide(){
	$("#otherSide").html("Back to normal map");
	$("#otherSide").click(function(){
		$("#map2").hide();
		$("#map2").html("");
		$("#map").css("height","500px");
		$("#map").css("width","700px");
		//init();
		$("#otherSide").html("Dig to other side of the world?");
		$("#otherSide").click(function(){initOtherSide();$("#map2").show();});
	});
	
}
